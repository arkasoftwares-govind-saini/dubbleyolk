using System;

namespace Gas
{
   public partial class LegacyCalculator
   {
      private class PlannedStart : IPlannedStart
      {

         #region Properties
         public DateTime StartTime { get; set; }
         public int Count { get; set; }
         #endregion

         #region Ctor
         public PlannedStart()
         {

         }
         public PlannedStart(DateTime starttime)
         {
            StartTime = starttime;
         }
         public PlannedStart(DateTime starttime, int count)
         {
            StartTime = starttime;
            Count = count;
         }
         #endregion
      }
   }
}
