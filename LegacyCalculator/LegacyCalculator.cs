﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gas
{
   public partial class LegacyCalculator
   {
      public IPlannedStart Calculate(List<DateTime> dates, int requiredDays = 1)
      {
         
         var plannedStart = new PlannedStart(DateTime.MinValue);
         // check if dates no items then return early
         if (dates.Count == 0)
         {
            return plannedStart;
         }

         dates = dates.OrderBy(d => d).ToList();

         var requiredNumberInFirstWeek = requiredDays;

         var startOfFirstWeek = dates[0];
         // add a week
         var startOfSecondWeek = startOfFirstWeek.AddDays(7);

         var countsForFirstWeek = dates
            .Count(x => x > startOfFirstWeek && x < startOfFirstWeek.AddDays(7)); // add a week

         var countsForSecondWeek = dates
            .Count(x => x > startOfSecondWeek && x < startOfSecondWeek.AddDays(7)); // add a week

         if (countsForSecondWeek > countsForFirstWeek && countsForSecondWeek >= requiredNumberInFirstWeek)
         {
            plannedStart = new PlannedStart(startOfSecondWeek, countsForSecondWeek);
         }
         return plannedStart;
      }
   }
}
